import math


def create_cave(min_x, max_x, max_y):
    cave = []
    for j in range(max_y):
        layer_j = []
        for i in range(max_x-min_x):
            layer_j.append(".")
        cave.append(layer_j)

    return cave


def create_cave_part_2(name, sandspawn):
    max_y= 0
    with open(name, "r") as file:
        for line in file.readlines():
            stone_ways = line.strip().split("->")
            for index in range(len(stone_ways)):
                max_y = max(max_y, int(stone_ways[index].split(",")[1]))

    cave_part_2 = create_cave(0, 1000, max_y+3)

    for index in range(len(cave_part_2[max_y+2])):
        cave_part_2[max_y+2][index] = "#"

    return cave_part_2, 0


def draw_way(cave, old_x, old_y, x, y):
    if old_x == x:
        for index in range(min(old_y, y), max(old_y, y)):
            cave[index][x] = "#"
    else:
        for index in range(min(old_x, x), max(old_x, x)):
            cave[y][index] = "#"


def get_stone_ways(name, sand_spawn, cave, min_x):
    with open(name, "r") as file:
        for line in file.readlines():
            stone_ways = line.strip().split("->")
            old_x, old_y = -1, -1
            for index in range(len(stone_ways)):
                x, y = int(stone_ways[index].split(",")[0]) - min_x, int(stone_ways[index].split(",")[1])
                cave[y][x] = "#"
                if old_x != -1 and old_y != -1:
                    draw_way(cave, old_x, old_y, x, y)
                old_y, old_x = y, x

    cave[0][sand_spawn-min_x] = "+"


def write_cave_to_file(name, cave):
    open(name, 'w').close()

    with open(name, "a") as file:
        for line in cave:
            file.write(str(line) + "\n")


def drop_sand(cave, sand_x, sand_y):
    while cave[sand_y][sand_x] != "o" and cave[sand_y][sand_x] != "#" and 0 <= sand_x < len(cave[0]) and 0 <= sand_y < len(cave)-1:
        if cave[sand_y+1][sand_x] == ".":
            sand_y += 1
            continue
        if cave[sand_y+1][sand_x-1] == ".":
            sand_y += 1
            sand_x -= 1
            continue
        if cave[sand_y+1][sand_x+1] == ".":
            sand_y += 1
            sand_x += 1
            continue
        cave[sand_y][sand_x] = "o"
        return True
    return False


def last_sand_corn(cave, sand_x, sand_y):
    while cave[sand_y][sand_x] != "o" and cave[sand_y][sand_x] != "#" and 0 <= sand_x < len(cave[0]) and 0 <= sand_y < len(cave)-1:
        cave[sand_y][sand_x] = "~"
        if cave[sand_y+1][sand_x] == ".":
            sand_y += 1
            continue
        if cave[sand_y+1][sand_x-1] == ".":
            sand_y += 1
            sand_x -= 1
            continue
        if cave[sand_y+1][sand_x+1] == ".":
            sand_y += 1
            sand_x += 1
            continue
        cave[sand_y][sand_x] = "o"
        return True
    return False


def spawn_sand(cave, sandspawn):
    count = 0
    while drop_sand(cave, sandspawn, 0):
        count += 1

    return count
def main():
    min_x, max_x, max_y, sandspawn = 450, 560, 170, 500
    cave = create_cave(min_x, max_x, max_y)
    get_stone_ways("cave_input", sandspawn, cave, min_x)
    count = spawn_sand(cave, sandspawn-min_x)
    last_sand_corn(cave, sandspawn-min_x, 0)
    write_cave_to_file("cave_output", cave)
    print("in part one ", count, " many sand corns fall")

    cave_part_2, min_x_part_2 = create_cave_part_2("cave_input", sandspawn)
    get_stone_ways("cave_input", sandspawn, cave_part_2, min_x_part_2)
    count_part_2 = spawn_sand(cave_part_2, sandspawn - min_x_part_2)
    write_cave_to_file("cave_output_part_2", cave_part_2)
    print("in part two ", count_part_2, " many sand corns fall")


if __name__ == "__main__":
    main()