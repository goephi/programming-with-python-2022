class Drop:
    x = int
    y = int
    z = int

    def __init__(self,x,y,z):
        self.x = x
        self.y = y
        self.z = z

    def adjacent_to(self,drop):
        if (self.x-drop.x)**2 == 1 and self.y==drop.y and self.z==drop.z:
            return True
        if self.x==drop.x and (self.y-drop.y)**2 == 1 and self.z==drop.z:
            return True
        if self.x==drop.x and self.y==drop.y and (self.z-drop.z)**2 == 1:
            return True
        return False

def read_file_to_lava(name):
    lava = []
    with open(name, "r") as file:
        for line in file.readlines():
            x, y, z = line.strip().split(",")
            drop = Drop(int(x),int(y),int(z))
            lava.append(drop)

    return lava


def get_surface(lava):
    surface = len(lava)*6
    for i in range(len(lava)):
        j = i+1
        while j < len(lava):
            if lava[i].adjacent_to(lava[j]):
                surface -= 2
            j += 1
    return surface


def test_lava_part_1():
    lava = read_file_to_lava("test_lava_part_1")
    surface = get_surface(lava)
    assert surface == 64

def main():
    lava = read_file_to_lava("Lavadropled")
    surface = get_surface(lava)
    print(surface)



if __name__ == "__main__":
    main()