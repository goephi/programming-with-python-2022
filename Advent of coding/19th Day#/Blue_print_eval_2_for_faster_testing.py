import copy


class BluePrint:
    number = int
    ore_robot_cost = int
    clay_robot_cost = int
    obsidian_robot_cost = [int, int]
    geode_robot_cost = [int, int]
    max_geodes = int

    def __init__(self, number, ore_robot_cost, clay_robot_cost, obsidian_robot_cost, geode_robot_cost):
        self.number = number
        self.ore_robot_cost = ore_robot_cost
        self.clay_robot_cost = clay_robot_cost
        self.obsidian_robot_cost = obsidian_robot_cost
        self.geode_robot_cost = geode_robot_cost
        self.max_geodes = 0


class Run:
    time_left = int
    ore_robot_count = int
    clay_robot_count = int
    obsidian_robot_count = int
    geode_robot_count = int
    ore_storage = int
    clay_storage = int
    obsidian_storage = int
    geode_storage = int

    def __init__(self, time_left):
        self.time_left = time_left
        self.ore_robot_count = 1
        self.clay_robot_count = 0
        self.obsidian_robot_count = 0
        self.geode_robot_count = 0
        self.ore_storage = 0
        self.clay_storage = 0
        self.obsidian_storage = 0
        self.geode_storage = 0

    def collect(self):
        self.ore_storage += self.ore_robot_count
        self.clay_storage += self.clay_robot_count
        self.obsidian_storage += self.obsidian_robot_count
        self.geode_storage += self.geode_robot_count

    def can_construct_ore_robot(self, blueprint):
        if blueprint.ore_robot_cost > self.ore_storage:
            return False
        else:
            return True

    def construct_ore_robot(self, blueprint):
        self.ore_storage -= blueprint.ore_robot_cost
        self.ore_robot_count += 1

    def can_construct_clay_robot(self, blueprint):
        if blueprint.clay_robot_cost > self.ore_storage:
            return False
        else:
            return True

    def construct_clay_robot(self, blueprint):
        self.ore_storage -= blueprint.clay_robot_cost
        self.clay_robot_count += 1

    def can_construct_obsidian_robot(self, blueprint):
        if blueprint.obsidian_robot_cost[0] > self.ore_storage or blueprint.obsidian_robot_cost[1] > self.clay_storage:
            return False
        else:
            return True

    def construct_obsidian_robot(self, blueprint):
        self.ore_storage -= blueprint.obsidian_robot_cost[0]
        self.clay_storage -= blueprint.obsidian_robot_cost[1]
        self.obsidian_robot_count += 1

    def can_construct_geode_robot(self, blueprint):
        if blueprint.geode_robot_cost[0] > self.ore_storage or blueprint.geode_robot_cost[1] > self.obsidian_storage:
            return False
        else:
            return True

    def construct_geode_robot(self, blueprint):
        self.ore_storage -= blueprint.geode_robot_cost[0]
        self.obsidian_storage -= blueprint.geode_robot_cost[1]
        self.geode_robot_count += 1

    def time_to_get_ore_robot(self, blueprint):
        for i in range(self.time_left):
            if self.ore_storage + i * self.ore_robot_count >= blueprint.ore_robot_cost:
                return i
        return self.time_left

    def time_to_get_clay_robot(self, blueprint):
        for i in range(self.time_left):
            if self.ore_storage + i * self.ore_robot_count >= blueprint.clay_robot_cost:
                return i
        return self.time_left

    def time_to_get_obsidian_robot(self, blueprint):
        for i in range(self.time_left):
            if self.ore_storage + i * self.ore_robot_count >= blueprint.obsidian_robot_cost[0] and \
                    self.clay_storage + i * self.clay_robot_count >= blueprint.obsidian_robot_cost[1]:
                return i
        return self.time_left

    def time_to_get_geode_robot(self, blueprint):
        for i in range(self.time_left):
            if self.ore_storage + i * self.ore_robot_count >= blueprint.geode_robot_cost[0] and \
                    self.obsidian_storage + i * self.obsidian_robot_count >= blueprint.geode_robot_cost[1]:
                return i
        return self.time_left


def read_blue_prints(name):
    blue_prints = []
    with open(name, "r") as file:
        for line in file.readlines():
            line = line.split()
            number = int(line[1].replace(":", ""))
            ore_cost = int(line[6])
            clay_cost = int(line[12])
            obsidian_cost = [int(line[18]), int(line[21])]
            geode_cost = [int(line[27]), int(line[30])]

            blueprint = BluePrint(number, ore_cost, clay_cost, obsidian_cost, geode_cost)
            blue_prints.append(blueprint)

    return blue_prints


def naive_one_step_algorithm_2(blue_print, run):
    runs = []
    if run.can_construct_geode_robot(blue_print):
        run_2 = copy.copy(run)
        run_2.collect()
        run_2.construct_geode_robot(blue_print)
        run_2.time_left -= 1
        runs.append(run_2)
    else:
        if run.can_construct_obsidian_robot(blue_print):
            run_2 = copy.copy(run)
            run_2.collect()
            run_2.construct_obsidian_robot(blue_print)
            run_2.time_left -= 1
            runs.append(run_2)
        if run.can_construct_clay_robot(blue_print):
            run_2 = copy.copy(run)
            run_2.collect()
            run_2.construct_clay_robot(blue_print)
            run_2.time_left -= 1
            if run.time_to_get_obsidian_robot(blue_print) > run_2.time_to_get_obsidian_robot(blue_print):
                runs.append(run_2)
        if run.can_construct_ore_robot(blue_print):
            run_2 = copy.copy(run)
            run_2.collect()
            run_2.construct_ore_robot(blue_print)
            run_2.time_left -= 1
            if run.time_to_get_geode_robot(blue_print) > run_2.time_to_get_geode_robot(blue_print) or \
                    run.time_to_get_obsidian_robot(blue_print) > run_2.time_to_get_obsidian_robot(blue_print) or \
                    run.time_to_get_clay_robot(blue_print) > run_2.time_to_get_clay_robot(blue_print) or \
                    run.time_to_get_ore_robot(blue_print) > run_2.time_to_get_ore_robot(blue_print):
                runs.append(run_2)
        else:
            run_2 = copy.copy(run)
            run_2.collect()
            run_2.time_left -= 1
            runs.append(run_2)

    return runs


def naive_controller_2(blueprint, time):
    runs = [Run(time)]

    while runs[0].time_left > 0:
        new_runs = naive_one_step_algorithm_2(blueprint, runs.pop(0))
        for e in new_runs:
            runs.append(e)

    result = 0
    for run in runs:
        result = max(result, run.geode_storage)

    return result


def calc_number_2(blueprints, time):
    count = 0
    for blueprint in blueprints:
        print(blueprint.number)
        count += naive_controller_2(blueprint, time) * blueprint.number

    return count


def test_naive():
    blueprints = read_blue_prints("test_blue_prints")
    result = naive_controller_2(blueprints[0], 32)
    assert result == 56


def test_naive_2():
    blueprints = read_blue_prints("test_blue_prints")
    result = naive_controller_2(blueprints[1], 32)
    assert result == 62

def test_naive_5():
    blueprints = read_blue_prints("test_blue_prints")
    result = calc_number_2(blueprints, 32)
    assert result == 56


def main():
    blueprints = read_blue_prints("BluePrints_2.0")
    print(calc_number_2(blueprints, 32))


if __name__ == "__main__":
    main()
