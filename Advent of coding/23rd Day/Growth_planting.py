import math


class Elf:
    x = int
    y = int
    directions = []
    future_x = int
    future_y = int

    def __init__(self, x, y):
        self.x = x
        self.y = y
        self.directions = ['N', 'S', 'W', 'E']
        self.future_x = x
        self.future_y = y

    def can_step_to(self, elfs, direction):
        if direction == 'N':
            for e in elfs:
                if (e.x == self.x - 1 or e.x == self.x or e.x == self.x + 1) and e.y == self.y + 1:
                    return False
        elif direction == 'S':
            for e in elfs:
                if (e.x == self.x - 1 or e.x == self.x or e.x == self.x + 1) and e.y == self.y - 1:
                    return False
        elif direction == 'W':
            for e in elfs:
                if (e.y == self.y - 1 or e.y == self.y or e.y == self.y + 1) and e.x == self.x - 1:
                    return False
        elif direction == 'E':
            for e in elfs:
                if (e.y == self.y - 1 or e.y == self.y or e.y == self.y + 1) and e.x == self.x + 1:
                    return False
        return True

    def needs_not_to_step(self, elfs):
        return self.can_step_to(elfs, 'N') and self.can_step_to(elfs, 'S') and self.can_step_to(elfs, 'W') and self.can_step_to(elfs, 'E')

    def calc_future_position_in_direction(self, direction):
        if direction == 'N':
            self.future_y = self.y + 1
        elif direction == 'S':
            self.future_y = self.y - 1
        elif direction == 'W':
            self.future_x = self.x - 1
        elif direction == 'E':
            self.future_x = self.x + 1
        return

    def get_future_position_for_elf(self, elfs):
        if not self.needs_not_to_step(elfs):
            for direction in self.directions:
                if self.can_step_to(elfs, direction):
                    self.calc_future_position_in_direction(direction)
                    break
        direction = self.directions.pop(0)
        self.directions.append(direction)
        return


def draw_elfs(elfs):
    min_y, min_x, max_y, max_x = math.inf, math.inf, -math.inf, -math.inf

    for e in elfs:
        min_x = min(e.x, min_x)
        min_y = min(e.y, min_y)
        max_x = max(e.x, max_x)
        max_y = max(e.y, max_y)

    out_put = ""
    for i in range(min_y, max_y + 1):
        for j in range(min_x, max_x + 1):
            is_elf = False
            for elf in elfs:
                if elf.x == j and elf.y == i:
                    is_elf = True
                    break
            if is_elf:
                out_put += '#'
            else:
                out_put += '.'
        out_put += '\n'

    print(out_put)


def perform_step(elfs):
    for index_1 in range(len(elfs)):
        go_to_y, go_to_x = elfs[index_1].future_y, elfs[index_1].future_x
        for index_2 in range(index_1 + 1, len(elfs)):
            if elfs[index_2].future_x == go_to_x and elfs[index_2].future_y == go_to_y:
                elfs[index_2].future_x = elfs[index_2].x
                elfs[index_2].future_y = elfs[index_2].y
                elfs[index_1].future_x = elfs[index_1].x
                elfs[index_1].future_y = elfs[index_1].y

    for e in elfs:
        e.x = e.future_x
        e.y = e.future_y

    return


def read_elfs_from_file(name):
    elfs = []
    with open(name, "r") as file:
        lines = file.readlines()
        y = len(lines)
        for line in lines:
            for x in range(len(line)):
                if line[x] == '#':
                    elf = Elf(x, y)
                    elfs.append(elf)
            y -= 1

    return elfs


def simulate_n_steps(elfs, n):
    #draw_elfs(elfs)
    for i in range(n):
        for e in elfs:
            e.get_future_position_for_elf(elfs)
        perform_step(elfs)
        #draw_elfs(elfs)
    return elfs


def calc_number_empty_ground_tiles_in_rectangle(elfs):
    min_y, min_x, max_y, max_x = math.inf, math.inf, -math.inf, -math.inf

    for e in elfs:
        min_x = min(e.x, min_x)
        min_y = min(e.y, min_y)
        max_x = max(e.x, max_x)
        max_y = max(e.y, max_y)

    size_x = max(max_x - min_x, (max_x - min_x) * -1) + 1
    size_y = max(max_y - min_y, (max_y - min_y) * -1) + 1

    size_rectangle = size_x * size_y

    #print(min_x, max_x, size_x, min_y, max_y, size_y, size_rectangle)

    return size_rectangle - len(elfs)

def count_num_of_steps(elfs):
    count = 1
    finished = True
    while True:
        print(count)
        for elf in elfs:
            if not elf.needs_not_to_step(elfs):
                finished = False
                break

        if finished:
            return count
        else:
            elfs = simulate_n_steps(elfs, 1)
            finished = True
            count += 1



def test_part_1():
    elfs = read_elfs_from_file("test_input")
    elfs = simulate_n_steps(elfs, 10)
    empty_ground_tiles = calc_number_empty_ground_tiles_in_rectangle(elfs)
    assert empty_ground_tiles == 110


def test_part_1_2():
    elfs = read_elfs_from_file("test_input")
    elfs = simulate_n_steps(elfs, 1)
    empty_ground_tiles = calc_number_empty_ground_tiles_in_rectangle(elfs)
    assert empty_ground_tiles == 59


def test_part_1_3():
    elfs = read_elfs_from_file("test_input")
    elfs = simulate_n_steps(elfs, 2)
    empty_ground_tiles = calc_number_empty_ground_tiles_in_rectangle(elfs)
    assert empty_ground_tiles == 77


def test_part_1_4():
    elfs = read_elfs_from_file("test_input")
    elfs = simulate_n_steps(elfs, 3)
    empty_ground_tiles = calc_number_empty_ground_tiles_in_rectangle(elfs)
    assert empty_ground_tiles == 88


def test_part_1_5():
    elfs = read_elfs_from_file("test_input")
    elfs = simulate_n_steps(elfs, 4)
    empty_ground_tiles = calc_number_empty_ground_tiles_in_rectangle(elfs)
    assert empty_ground_tiles == 88


def test_part_1_6():
    elfs = read_elfs_from_file("test_input")
    elfs = simulate_n_steps(elfs, 5)
    empty_ground_tiles = calc_number_empty_ground_tiles_in_rectangle(elfs)
    assert empty_ground_tiles == 99


def test_2_part_1():
    elfs = read_elfs_from_file("test_input_2")
    elfs = simulate_n_steps(elfs, 1)
    empty_ground_tiles = calc_number_empty_ground_tiles_in_rectangle(elfs)
    assert empty_ground_tiles == 5


def test_2_2_part_1():
    elfs = read_elfs_from_file("test_input_2")
    elfs = simulate_n_steps(elfs, 3)
    empty_ground_tiles = calc_number_empty_ground_tiles_in_rectangle(elfs)
    assert empty_ground_tiles == 25


def test_2_3_part_1():
    elfs = read_elfs_from_file("test_input_2")
    elfs = simulate_n_steps(elfs, 2)
    empty_ground_tiles = calc_number_empty_ground_tiles_in_rectangle(elfs)
    assert empty_ground_tiles == 15

def test_part_2():
    elfs = read_elfs_from_file("test_input")
    count = count_num_of_steps(elfs)
    assert count == 20


def main():
    elfs = read_elfs_from_file("elfes_input")
    elfs = simulate_n_steps(elfs, 10)
    empty_ground_tiles = calc_number_empty_ground_tiles_in_rectangle(elfs)
    print("part 1 is ", empty_ground_tiles, " many empty ground tiles after 10 steps")

    elfs_2 = read_elfs_from_file("elfes_input")
    count = count_num_of_steps(elfs_2)
    print("part 2 is ", empty_ground_tiles, " steps until they are all at place")


if __name__ == "__main__":
    main()
