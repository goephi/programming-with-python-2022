class Board:
    breadth = int
    height = int
    tower_height = int
    field = []

    def __init__(self, breadth):
        self.breadth = breadth
        self.height = 0
        self.tower_height = 0
        self.field = [["."] * breadth]

    def get_field(self, x, y):
        if y >= len(self.field):
            return '.'
        return self.field[y][x]

    def increase_height(self):
        while self.tower_height + 3 > self.height:
            self.field.append(["."] * self.breadth)
            self.height += 1

    def add_piece(self, piece, x, y):
        if piece == "-":
            self.field[y][x] = '#'
            self.field[y][x + 1] = '#'
            self.field[y][x + 2] = '#'
            self.field[y][x + 3] = '#'
        if piece == "+":
            self.field[y][x + 1] = '#'
            self.field[y + 1][x] = '#'
            self.field[y + 1][x + 1] = '#'
            self.field[y + 1][x + 2] = '#'
            self.field[y + 2][x + 1] = '#'
        if piece == "J":
            self.field[y][x] = '#'
            self.field[y][x + 1] = '#'
            self.field[y][x + 2] = '#'
            self.field[y + 1][x + 2] = '#'
            self.field[y + 2][x + 2] = '#'
        if piece == "I":
            self.field[y][x] = '#'
            self.field[y + 1][x] = '#'
            self.field[y + 2][x] = '#'
            self.field[y + 3][x] = '#'
        if piece == "O":
            self.field[y][x] = '#'
            self.field[y][x + 1] = '#'
            self.field[y + 1][x] = '#'
            self.field[y + 1][x + 1] = '#'

        self.tower_height = max(self.tower_height, y + get_piece_height(piece))


def Pieces():
    while True:
        yield "-"
        yield "+"
        yield "J"
        yield "I"
        yield "O"


def Gases(gas):
    index = -1
    while True:
        index += 1
        yield gas[index % len(gas)]



def get_piece_breadth(piece):
    if piece == "-":
        return 4
    if piece == "+":
        return 3
    if piece == "J":
        return 3
    if piece == "I":
        return 1
    if piece == "O":
        return 2


def get_piece_height(piece):
    if piece == "-":
        return 1
    if piece == "+":
        return 3
    if piece == "J":
        return 3
    if piece == "I":
        return 4
    if piece == "O":
        return 2


def move_possible_gas(board, piece, x, y, gas):
    if gas == "<":
        if x == 0:
            return False
        if piece == "-":
            if board.get_field(x - 1, y) == "#":
                return False
            return True
        if piece == "+":
            if board.get_field(x, y) == "#":
                return False
            if board.get_field(x-1, y+1) == '#':
                return False
            if board.get_field(x, y+2) == '#':
                return False
            return True
        if piece == "J":
            if board.get_field(x-1, y) == '#':
                return False
            if board.get_field(x+1, y+1) == '#':
                return False
            if board.get_field(x+1, y+2) == '#':
                return False
            return True
        if piece == "I":
            if board.get_field(x-1, y) == '#':
                return False
            if board.get_field(x-1, y+1) == '#':
                return False
            if board.get_field(x-1, y+2) == '#':
                return False
            if board.get_field(x-1, y+3) == '#':
                return False
            return True
        if piece == "O":
            if board.get_field(x-1, y) == '#':
                return False
            if board.get_field(x-1, y+1) == '#':
                return False
            return True

    if gas == ">":
        if x + get_piece_breadth(piece) >= board.breadth:
            return False
        if piece == "-":
            if board.get_field(x+4, y) == "#":
                return False
            return True
        if piece == "+":
            if board.get_field(x+2, y) == "#":
                return False
            if board.get_field(x+3, y+1) == '#':
                return False
            if board.get_field(x+2, y+2) == '#':
                return False
            return True
        if piece == "J":
            if board.get_field(x+3, y) == '#':
                return False
            if board.get_field(x+3, y+1) == '#':
                return False
            if board.get_field(x+3, y+2) == '#':
                return False
            return True
        if piece == "I":
            if board.get_field(x+1, y) == '#':
                return False
            if board.get_field(x+1, y+1) == '#':
                return False
            if board.get_field(x+1, y+2) == '#':
                return False
            if board.get_field(x+1, y+3) == '#':
                return False
            return True
        if piece == "O":
            if board.get_field(x+2, y) == '#':
                return False
            if board.get_field(x+2, y+1) == '#':
                return False
            return True


def move_possible_down(board, piece, x, y):
    if y == 0:
        return False
    if piece == "-":
        if board.get_field(x, y-1) == '#':
            return False
        if board.get_field(x+1, y-1) == '#':
            return False
        if board.get_field(x+2, y-1) == '#':
            return False
        if board.get_field(x+3, y-1) == '#':
            return False
        return True
    if piece == "+":
        if board.get_field(x, y) == '#':
            return False
        if board.get_field(x+1, y-1) == '#':
            return False
        if board.get_field(x+2, y) == '#':
            return False
        return True
    if piece == "J":
        if board.get_field(x, y-1) == '#':
            return False
        if board.get_field(x+1, y-1) == '#':
            return False
        if board.get_field(x+2, y-1) == '#':
            return False
        return True
    if piece == "I":
        if board.get_field(x, y-1) == '#':
            return False
        return True
    if piece == "O":
        if board.get_field(x, y-1) == '#':
            return False
        if board.get_field(x+1, y-1) == '#':
            return False
        return True


def run_algorithm(iterations, Gas, board):
    pieces = Pieces()
    gases = Gases(Gas)

    for index in range(iterations):
        print(index/ iterations)
        piece = next(pieces)
        board.increase_height()
        x_position, y_position = 2, board.tower_height + 3

        while True:
            gas = next(gases)
            if move_possible_gas(board, piece, x_position, y_position, gas):
                if gas == "<":
                    x_position -= 1
                if gas == ">":
                    x_position += 1

            if move_possible_down(board, piece, x_position, y_position):
                y_position -= 1
            else:
                board.add_piece(piece, x_position, y_position)
                break

def read_Gas(name):
    with open(name, "r") as file:
        return file.readline().strip()

def test_part_1():
    board = Board(7)
    gas = read_Gas("test_part_1")
    run_algorithm(2022, gas, board)
    for l in board.field:
        print(l)
    assert board.tower_height == 3068

def test_part_1_2():
    board = Board(7)
    gas = read_Gas("test_part_1")
    run_algorithm(1, gas, board)
    for l in board.field:
        print(l)
    assert board.tower_height == 1
def test_part_1_3():
    board = Board(7)
    gas = read_Gas("test_part_1")
    run_algorithm(5, gas, board)
    for l in board.field:
        print(l)
    assert board.tower_height == 9
def test_part_1_4():
    board = Board(7)
    gas = read_Gas("test_part_1")
    run_algorithm(10, gas, board)
    for l in board.field:
        print(l)
    assert board.tower_height == 17

def test_part_1_5():
    board = Board(7)
    gas = read_Gas("test_part_1")
    run_algorithm(1000000000000, gas, board)
    assert board.tower_height == 1514285714288


def main():
    board = Board(7)
    gas = read_Gas("Gas")
    run_algorithm(2022, gas, board)
    print("part 1 tower height of ", board.tower_height)

    board_2 = Board(7)
    gas_2 = read_Gas("Gas")
    run_algorithm(1000000000000, gas_2, board_2)
    print("part 2 tower height of ", board_2.tower_height)



if __name__ == "__main__":
    main()
