
class Monkey:
    items = []
    worry_add = int
    worry_mul = str
    test_div_by = int
    if_test_true = int
    if_test_false = int

    def __init__(self, items, worry_add, worry_mul, test_div_by, if_test_true, if_test_false):
        self.items = items
        self.worry_add = worry_add
        self.worry_mul = worry_mul
        self.test_div_by = test_div_by
        self.if_test_true = if_test_true
        self.if_test_false = if_test_false

    def add_item(self, item):
        self.items.append(item)

    def has_items_left(self):
        return len(self.items) != 0

    def throw_item(self):
        item = self.items.pop(0)
        worry = item + self.worry_add

        if self.worry_mul == "old":
            worry *= worry
        elif self.worry_mul != "":
            worry *= int(self.worry_mul)

        worry = worry // 3

        if worry % self.test_div_by == 0:
            return self.if_test_true, worry
        else:
            return self.if_test_false, worry

    def throw_item_no_worry(self):
        item = self.items.pop(0)
        worry = item + self.worry_add

        if self.worry_mul == "old":
            worry *= worry
        elif self.worry_mul != "":
            worry *= int(self.worry_mul)

        if worry % self.test_div_by == 0:
            return self.if_test_true, worry
        else:
            return self.if_test_false, worry


def read_file_to_monkeys(name, num_monkeys):
    monkeys = []
    with open(name, "r") as file:
        for i in range(num_monkeys):
            line = file.readline()
            if line.startswith("Monkey"):
                items, worry_add, worry_mul, test_div_by, if_test_true, if_test_false = [], 0, "", 1, 0, 0
                line = file.readline().strip().split()
                index = 2
                while index < len(line):
                    items.append(int(line[index].replace(",", "")))
                    index += 1

                line = file.readline().strip().split()
                if line[4] == "+":
                    worry_add = int(line[5])
                elif line[4] == "*":
                    worry_mul = line[5]

                line = file.readline().strip().split()
                test_div_by = int(line[3])

                line = file.readline().strip().split()
                if_test_true = int(line[5])

                line = file.readline().strip().split()
                if_test_false = int(line[5])

                file.readline()

                new_Monkey = Monkey(items, worry_add, worry_mul, test_div_by, if_test_true, if_test_false)
                monkeys.append(new_Monkey)

    return monkeys

def play_n_rounds(monkeys, n):
    inspection_count = []
    for m in monkeys:
        inspection_count.append(0)

    for index in range(n):
        for monkey in range(len(monkeys)):
            while monkeys[monkey].has_items_left():
                to, item = monkeys[monkey].throw_item()
                monkeys[to].add_item(item)
                inspection_count[monkey] += 1

    return inspection_count

def play_n_rounds_part_2(monkeys, n):
    inspection_count = []
    for m in monkeys:
        inspection_count.append(0)

    for index in range(n):
        print(index)
        for monkey in range(len(monkeys)):
            while monkeys[monkey].has_items_left():
                to, item = monkeys[monkey].throw_item_no_worry()
                monkeys[to].add_item(item)
                inspection_count[monkey] += 1

    return inspection_count

def eval_inspections(inspection_count):
    max_1, max_2 = 0, 0
    for val in inspection_count:
        if val > max_1:
            max_2 = max_1
            max_1 = val
        elif val > max_2:
            max_2 = val

    return max_1 * max_2


def test_monkeys():
    test_monkeys = read_file_to_monkeys("Monkey_test", 4)
    test_inspection_count = play_n_rounds(test_monkeys, 20)
    assert test_inspection_count == [101, 95, 7, 105]
    test_final = eval_inspections(test_inspection_count)
    assert test_final == 10605


def main():
    monkeys = read_file_to_monkeys("Monkey_input", 8)
    inspection_count = play_n_rounds(monkeys, 20)
    final = eval_inspections(inspection_count)
    print("part_1: the level of monkey business after 20 rounds of stuff-slinging simian shenanigans is: ", final)

    monkeys_part_2 = read_file_to_monkeys("Monkey_input", 8)
    inspection_count_part_2 = play_n_rounds_part_2(monkeys_part_2, 10000)
    final_part_2 = eval_inspections(inspection_count_part_2)
    print("part_2: the level of monkey business after 10000 rounds of stuff-slinging simian shenanigans is: ", final_part_2)





if __name__ == "__main__":
    main()