import copy


class Coordinate:
    value = int
    id = int

    def __init__(self, value, id):
        self.value = value
        self.id = id


def read_file_to_coordinate_list(name):
    coordinates = []
    count = 0
    with open(name, "r") as file:
        for line in file.readlines():
            number = int(line.strip())
            coordinates.append(Coordinate(number, count))
            count += 1
    return coordinates


def shuffle_coordinates(coordinates):
    new_list = []
    length_list = len(coordinates)
    index = 0

    while index < length_list:
        element = coordinates[index]
        if element.id in new_list or element.value == 0:
            index += 1
            continue
        else:
            new_list.append(element.id)

        coordinates.pop(index)

        if element.value > 0:
            new_index = index + element.value
        else:
            new_index = index + element.value
        new_index = correct_to_list_for_insert(new_index, length_list-1)
        if new_index > index:
            coordinates.insert(new_index, element)
        else:
            coordinates.insert(new_index, element)

    return coordinates


def correct_to_list_for_insert(new_index, length_list):
    while new_index <= 0:
        new_index += length_list

    if new_index >= length_list:
        new_index %= length_list
    return new_index


def find_3_values(coordinates):
    zero = 0
    length_list = len(coordinates)
    for index in range(length_list):
        if coordinates[index].value == 0:
            zero = index
            break

    thousand = coordinates[(zero + 1000) % length_list].value
    two_thousand = coordinates[(zero + 2000) % length_list].value
    three_thousand = coordinates[(zero + 3000) % length_list].value


    return sum([thousand, two_thousand, three_thousand])


def test_1():
    coordinates = read_file_to_coordinate_list("test_file")
    decoded_coordinates = shuffle_coordinates(coordinates)
    code = find_3_values(decoded_coordinates)
    assert code == 3
    print("part one evaluates to ", code)

def test_2():
    coordinates = [Coordinate(0, 0), Coordinate(1, 1), Coordinate(2, 2), Coordinate(1, 3), Coordinate(4, 4),
                   Coordinate(1, 5)]
    decoded_coordinates = shuffle_coordinates(coordinates)
    code = find_3_values(decoded_coordinates)
    assert code == 5
    print("part one evaluates to ", code)


def test_3():
    coordinates = [Coordinate(0, 0), Coordinate(1, 1), Coordinate(2, 2), Coordinate(3, 3), Coordinate(4, 4),
                   Coordinate(5, 5)]
    decoded_coordinates = shuffle_coordinates(coordinates)
    code = find_3_values(decoded_coordinates)
    assert code == 9
    print("part one evaluates to ", code)

def test_4():
    coordinates = [Coordinate(0, 0), Coordinate(2, 1), Coordinate(2, 2), Coordinate(2, 3), Coordinate(4, 4),
                   Coordinate(5, 5)]
    decoded_coordinates = shuffle_coordinates(coordinates)
    code = find_3_values(decoded_coordinates)
    assert code == 4
    print("part one evaluates to ", code)


def main():
    coordinates = read_file_to_coordinate_list("Coordinate_input")
    decoded_coordinates = shuffle_coordinates(coordinates)
    code = find_3_values(decoded_coordinates)
    print("part one evaluates to ", code)


if __name__ == "__main__":
    main()
