import copy


class Coordinate:
    value = int
    id = int

    def __init__(self, value, id):
        self.value = value
        self.id = id


def read_file_to_coordinate_list(name):
    coordinates = []
    count = 0
    with open(name, "r") as file:
        for line in file.readlines():
            number = int(line.strip()) * 811589153
            coordinates.append(Coordinate(number, count))
            count += 1
    return coordinates


def shuffle_coordinates(coordinates):

    for index in range(len(coordinates)):
        for element in coordinates:
            if element.id == index:
                if element.value == 0:
                    break

                list_index_of_element = coordinates.index(element)
                coordinates.pop(list_index_of_element)

                if element.value > 0:
                    new_index = list_index_of_element + element.value
                else:
                    new_index = list_index_of_element + element.value
                new_index = correct_to_list_for_insert(new_index, len(coordinates))
                coordinates.insert(new_index, element)

                break

    return coordinates


def number_of_shuffles(coordinates, n):
    for i in range(n):
        coordinates = shuffle_coordinates(coordinates)

    return coordinates


def correct_to_list_for_insert(new_index, length_list):
    if new_index <= 0:
        multiple = int((new_index / length_list)) * -1 + 1
        new_index += length_list * multiple

    if new_index >= length_list:
        new_index %= length_list
    return new_index


def find_3_values(coordinates):
    zero = 0
    length_list = len(coordinates)
    for index in range(length_list):
        if coordinates[index].value == 0:
            zero = index
            break

    thousand = coordinates[(zero + 1000) % length_list].value
    two_thousand = coordinates[(zero + 2000) % length_list].value
    three_thousand = coordinates[(zero + 3000) % length_list].value

    return sum([thousand, two_thousand, three_thousand])


def test_1():
    coordinates = read_file_to_coordinate_list("test_file")
    decoded_coordinates = shuffle_coordinates(coordinates)
    code = find_3_values(decoded_coordinates)
    assert code == 4869534918
    print("part one evaluates to ", code)


def test_2():
    coordinates = [Coordinate(0, 0), Coordinate(1, 1), Coordinate(2, 2), Coordinate(1, 3), Coordinate(4, 4),
                   Coordinate(1, 5)]
    decoded_coordinates = shuffle_coordinates(coordinates)
    code = find_3_values(decoded_coordinates)
    assert code == 5
    print("part one evaluates to ", code)


def test_3():
    coordinates = [Coordinate(0, 0), Coordinate(1, 1), Coordinate(2, 2), Coordinate(3, 3), Coordinate(4, 4),
                   Coordinate(5, 5)]
    decoded_coordinates = shuffle_coordinates(coordinates)
    code = find_3_values(decoded_coordinates)
    assert code == 9
    print("part one evaluates to ", code)


def test_4():
    coordinates = [Coordinate(0, 0), Coordinate(2, 1), Coordinate(2, 2), Coordinate(2, 3), Coordinate(4, 4),
                   Coordinate(5, 5)]
    decoded_coordinates = shuffle_coordinates(coordinates)
    code = find_3_values(decoded_coordinates)
    assert code == 4
    print("part one evaluates to ", code)


def test_5():
    coordinates = read_file_to_coordinate_list("test_file")
    assert coordinates[0].value == 811589153
    assert coordinates[1].value == 1623178306
    assert coordinates[2].value == -2434767459
    assert coordinates[3].value == 2434767459
    assert coordinates[4].value == -1623178306
    assert coordinates[5].value == 0
    assert coordinates[6].value == 3246356612

    decoded_coordinates = number_of_shuffles(coordinates, 1)
    assert decoded_coordinates[0].value == 0
    assert decoded_coordinates[1].value == -2434767459
    assert decoded_coordinates[2].value == 3246356612
    assert decoded_coordinates[3].value == -1623178306
    assert decoded_coordinates[4].value == 2434767459
    assert decoded_coordinates[5].value == 1623178306
    assert decoded_coordinates[6].value == 811589153

def test_6():
    coordinates = read_file_to_coordinate_list("test_file")
    decoded_coordinates = number_of_shuffles(coordinates, 10)
    score = find_3_values(decoded_coordinates)

    assert score == 1623178306



def main():
    coordinates = read_file_to_coordinate_list("Coordinate_input")
    decoded_coordinates = number_of_shuffles(coordinates, 10)
    code = find_3_values(decoded_coordinates)
    print("part two evaluates to ", code)


if __name__ == "__main__":
    main()
