class My_file(object):
    name = str
    size = int

    def __init__(self, name, size):
        self.name = name
        self.size = size

    def __str__(self):
        return f"{self.name} {self.size}"

    def get_size(self):
        return self.size


class My_directory(object):
    name = str
    directories = dict()
    files = list()

    def __init__(self, name, directories, files):
        self.name = name
        self.directories = directories
        self.files = files

    def add_item(self, item, position, dirs):
        position -= 1
        if position >= 1:
            self.directories[dirs[len(dirs) - position]].add_item(item, position, dirs)
        else:
            if item[0] == "dir":
                self.directories.update({item[1]: My_directory(item[1], {}, [])})
            else:
                self.files.append(My_file(item[1], int(item[0])))

    def __str__(self):
        return f"{self.name} {self.directories} {self.files}"

    def get_size(self):
        sum = 0
        for file in self.files:
            sum += file.get_size()
        for dir in self.directories:
            sum += self.directories[dir].get_size()

        return sum


def lines():
    with open("Directory_input", "r") as file:
        for line in file.readlines():
            yield line


def len_file():
    with open("Directory_input", "r") as file:
        return len(file.readlines())


def read_file_to_directory_layout():
    layout = My_directory("/", {}, [])
    current_position = []
    lines_read = lines()

    for index in range(len_file()):
        line = next(lines_read).split()
        if line[0] == "$":
            if line[1] == "cd":
                if line[2] == "..":
                    current_position.pop()
                else:
                    current_position.append(line[2])
        else:
            layout.add_item(line, len(current_position), current_position)

    return layout


def get_sum_size_over_all_Directories_smaller_100000(current_dir, sum):
    size = current_dir.get_size()
    if size < 100000:
        sum += size
    for e in current_dir.directories:
        sum = get_sum_size_over_all_Directories_smaller_100000(current_dir.directories[e], sum)

    return sum

def find_smallest_directory_to_get_to_30000000(current_dir, to_cut, minimal, name_of_min):
    current_dir_size = current_dir.get_size()
    if to_cut <= current_dir_size < minimal:
        minimal, name_of_min = current_dir_size, current_dir.name
    for dirs in current_dir.directories:
        result_1, result_2 = find_smallest_directory_to_get_to_30000000(current_dir.directories[dirs], to_cut, minimal, name_of_min)
        if result_1 < minimal:
            minimal, name_of_min = result_1, result_2

    return minimal, name_of_min

def main():
    layout = read_file_to_directory_layout()
    sum = get_sum_size_over_all_Directories_smaller_100000(layout, 0)
    print(sum)

    layout_size = layout.get_size()
    to_cut = layout_size - 70000000 + 30000000
    print("to cut ", to_cut)
    minimal = layout_size
    directory = "/"
    minimal, directory = find_smallest_directory_to_get_to_30000000(layout, to_cut, minimal, directory)
    print("directory to cut is ", directory, " with size ", minimal)

if __name__ == "__main__":
    main()
