import math


class Field:
    height = int
    x = int
    y = int
    path = int
    connected_to = []
    start = bool
    end = bool

    def __init__(self, height, x, y, path, start, end):
        self.height = height
        self.x = x
        self.y = y
        self.path = path
        self.start = start
        self.end = end

    def visited(self):
        return self.path != 0 or self.start

    def length_path(self):
        return self.path

    def can_reach(self, field):
        if self.x - field.x > 1 or self.x - field.x < -1:
            return False
        if self.y - field.y > 1 or self.y - field.y < -1:
            return False
        if (self.x - field.x == 1 or self.x - field.x == -1) and (self.y - field.y == 1 or self.y - field.y == -1):
            return False
        if self.height - field.height < -1:
            return False
        return True


def calc_connected_to(heightmap):
    for field in heightmap:
        connections = []
        for element in heightmap:
            if field != element and field.can_reach(element):
                connections.append(element)
        field.connected_to = connections


def read_file_to_heightmap(name):
    heightmap = []
    with open(name, "r") as file:
        lines = file.readlines()
        for y in range(len(lines)):
            for x in range(len(lines[y])):
                end, start, height = False, False, ord(lines[y][x]) - 97
                if lines[y][x] == "S":
                    start = True
                    height = 0
                elif lines[y][x] == "E":
                    end = True
                    height = 25
                field = Field(height, x, y, 0, start, end)
                heightmap.append(field)

    return heightmap


def read_file_to_heightmap_part_2(name):
    heightmap = []
    with open(name, "r") as file:
        lines = file.readlines()
        for y in range(len(lines)):
            for x in range(len(lines[y])):
                end, start, height = False, False, ord(lines[y][x]) - 97
                if lines[y][x] == "S" or lines[y][x] == "a":
                    start = True
                    height = 0
                elif lines[y][x] == "E":
                    end = True
                    height = 25
                field = Field(height, x, y, 0, start, end)
                heightmap.append(field)

    return heightmap


def breadth_first_search(queue):
    while len(queue) != 0:
        element = queue.pop(0)
        for field in element.connected_to:
            if not field.visited():
                field.path = element.path + 1
                if field.end:
                    return
                queue.append(field)


def start_iteration(heightmap):
    for field in heightmap:
        if field.start:
            breadth_first_search([field])


def start_iteration_part_2(heightmap):
    starts = []
    for field in heightmap:
        if field.start:
            starts.append(field)
    breadth_first_search(starts)


def find_path_to_end(heightmap):
    for field in heightmap:
        if field.end:
            return field.path


def test_heightmap():
    test_heightmap = read_file_to_heightmap("test_heightmap")
    calc_connected_to(test_heightmap)
    start_iteration(test_heightmap)
    test_result = find_path_to_end(test_heightmap)
    assert test_result == 31


def test_heightmap_2():
    test_heightmap_2 = read_file_to_heightmap("Test_heightmap_2")
    calc_connected_to(test_heightmap_2)
    start_iteration(test_heightmap_2)
    test_result = find_path_to_end(test_heightmap_2)
    assert test_result == 27


def test_field():
    test_field_1 = Field(0, 1, 1, 0, True, True)
    test_field_2 = Field(0, 1, 0, 0, False, False)
    test_field_3 = Field(0, 0, 1, 0, False, False)
    test_field_4 = Field(0, 1, 2, 0, False, False)
    test_field_5 = Field(0, 2, 1, 0, False, False)
    test_field_6 = Field(0, 0, 0, 0, False, False)
    test_field_7 = Field(0, 2, 0, 0, False, False)
    test_field_8 = Field(0, 0, 2, 0, False, False)
    test_field_9 = Field(0, 2, 2, 0, False, False)
    test_field_10 = Field(2, 1, 0, 0, False, False)
    test_field_11 = Field(2, 0, 1, 0, False, False)
    test_field_12 = Field(2, 1, 2, 0, False, False)
    test_field_13 = Field(2, 2, 1, 0, False, False)
    test_field_14 = Field(-1, 0, 1, 0, False, False)
    test_field_15 = Field(-1, 1, 0, 0, False, False)
    test_field_16 = Field(-1, 2, 1, 0, False, False)
    test_field_17 = Field(-1, 1, 2, 0, False, False)

    assert test_field_1.can_reach(test_field_2)
    assert test_field_1.can_reach(test_field_3)
    assert test_field_1.can_reach(test_field_4)
    assert test_field_1.can_reach(test_field_5)
    assert not test_field_1.can_reach(test_field_6)
    assert not test_field_1.can_reach(test_field_7)
    assert not test_field_1.can_reach(test_field_8)
    assert not test_field_1.can_reach(test_field_9)
    assert not test_field_1.can_reach(test_field_10)
    assert not test_field_1.can_reach(test_field_11)
    assert not test_field_1.can_reach(test_field_12)
    assert not test_field_1.can_reach(test_field_13)
    assert test_field_1.can_reach(test_field_14)
    assert test_field_1.can_reach(test_field_15)
    assert test_field_1.can_reach(test_field_16)
    assert test_field_1.can_reach(test_field_17)

def test_part_2():
    heightmap = read_file_to_heightmap_part_2("test_heightmap")
    calc_connected_to(heightmap)
    start_iteration_part_2(heightmap)
    result = find_path_to_end(heightmap)
    assert result == 29


def main():
    heightmap = read_file_to_heightmap("heightmap")
    calc_connected_to(heightmap)
    start_iteration(heightmap)
    end_path = find_path_to_end(heightmap)
    print(end_path)

    heightmap_part_2 = read_file_to_heightmap_part_2("heightmap")
    calc_connected_to(heightmap_part_2)
    start_iteration_part_2(heightmap_part_2)
    end_path_part_2 = find_path_to_end(heightmap_part_2)
    print(end_path_part_2)




if __name__ == "__main__":
    main()
