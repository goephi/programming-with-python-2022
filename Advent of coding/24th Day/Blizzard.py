class Blizzard:
    x = int
    y = int
    direction = str

    def __init__(self, x, y, direction):
        self.x = x
        self.y = y
        self.direction = direction

    def move(self, board):
        if self.direction == ">":
            self.x += 1
            if self.x >= board.x-1:
                self.x = 1
        elif self.direction == "<":
            self.x -= 1
            if self.x <= 0:
                self.x = board.x - 2
        elif self.direction == "^":
            self.y -= 1
            if self.y <= 0:
                self.y = board.y - 2
        elif self.direction == "v":
            self.y += 1
            if self.y >= board.y-1:
                self.y = 1


class Board:
    x = int
    y = int
    start = int
    end = int

    def __init__(self, x, y, start, end):
        self.x = x
        self.y = y
        self.start = start
        self.end = end


def read_file(name):
    blizzards = []
    with open(name, "r") as file:
        lines = file.readlines()
        board = Board(len(lines[0].strip()), len(lines), lines[0].find("."), lines[len(lines) - 1].find("."))
        for y in range(1, len(lines) - 1):
            for x in range(1, len(lines[y]) - 1):
                if lines[y][x] != ".":
                    blizzard = Blizzard(x, y, lines[y][x])
                    blizzards.append(blizzard)

    return blizzards, board


def check_move(coordinates, blizzards, board):
    new_coordinates = []
    new_coordinates.append([coordinates[0], coordinates[1]])
    new_coordinates.append([coordinates[0] - 1, coordinates[1]])
    new_coordinates.append([coordinates[0] + 1, coordinates[1]])
    new_coordinates.append([coordinates[0], coordinates[1] - 1])
    new_coordinates.append([coordinates[0], coordinates[1] + 1])

    index, length = 0, len(new_coordinates)

    while index < length:
        removed = False
        if new_coordinates[index][0] <= 0 or new_coordinates[index][0] >= board.x-1:
            new_coordinates.remove(new_coordinates[index])
            removed = True
        elif new_coordinates[index][1] == 0 and new_coordinates[index][0] != board.start:
            new_coordinates.remove(new_coordinates[index])
            removed = True
        elif new_coordinates[index][1] < 0:
            new_coordinates.remove(new_coordinates[index])
            removed = True
        elif new_coordinates[index][1] == board.y-1 and new_coordinates[index][0] != board.end:
            new_coordinates.remove(new_coordinates[index])
            removed = True
        elif new_coordinates[index][1] > board.y-1:
            new_coordinates.remove(new_coordinates[index])
            removed = True
        else:
            for blizzard in blizzards:
                if blizzard.x == new_coordinates[index][0] and blizzard.y == new_coordinates[index][1]:
                    new_coordinates.remove(new_coordinates[index])
                    removed = True
                    break
        if not removed:
            index += 1
        else:
            length -= 1

    return new_coordinates


def algorithm(name):
    blizzards, board = read_file(name)
    #print(board.x, board.y, board.start, board.end)
    queue = [[board.start, 0]]
    count = 0

    while len(queue) > 0:
        print(count)
        for blizzard in blizzards:
            blizzard.move(board)
        new_queue = []
        for element in queue:
            if element[0] == board.end and element[1] == board.y-1:
                return count
            for list_element in check_move(element, blizzards, board):
                if not list_element in new_queue:
                    new_queue.append(list_element)


        queue = new_queue
        count += 1

    return count


def test_part_1():
    count = algorithm("test")
    assert count == 18

def main():
    count = algorithm("input")


if __name__ == "__main__":
    main()
