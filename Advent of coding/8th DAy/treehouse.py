
def read_forest_file():
    forest = []
    with open("trees_imput", "r") as file:
        for line in file.readlines():
            chars = []
            for letter in line.strip():
                chars.append(letter)
            forest.append(chars)

    return forest

def visible_from_north(forest, tree):
    visible, index = True, 0
    while index < tree[1] and visible:
        visible = forest[tree[1]][tree[0]] > forest[index][tree[0]]
        index += 1

    return visible


def visible_from_south(forest, tree):
    visible, index = True, len(forest)-1
    while index > tree[1] and visible:
        visible = forest[tree[1]][tree[0]] > forest[index][tree[0]]
        index -= 1

    return visible


def visible_from_west(forest, tree):
    visible, index = True, 0
    while index < tree[0] and visible:
        visible = forest[tree[1]][tree[0]] > forest[tree[1]][index]
        index += 1

    return visible


def visible_from_east(forest, tree):
    visible, index = True, len(forest[tree[1]])-1
    while index > tree[0] and visible:
        visible = forest[tree[1]][tree[0]] > forest[tree[1]][index]
        index -= 1

    return visible



def is_tree_visible(forest, tree):
    return visible_from_north(forest, tree) or visible_from_east(forest, tree) or visible_from_south(forest, tree) or visible_from_west(forest, tree)

def count_visible_trees(forest):
    count = 0
    for y in range(len(forest)):
        for x in range(len(forest[y])):
            if is_tree_visible(forest, [x, y]):
                count += 1

    return count

def view_distance_north(forest, tree):
    distance, index, view = 0, tree[1]-1, True
    while index >= 0 and view:
        distance += 1
        view = forest[index][tree[0]] < forest[tree[1]][tree[0]]
        index -= 1

    return distance

def view_distance_south(forest, tree):
    distance, index, view = 0, tree[1]+1, True
    while index < len(forest) and view:
        distance += 1
        view = forest[index][tree[0]] < forest[tree[1]][tree[0]]
        index += 1

    return distance

def view_distance_east(forest, tree):
    distance, index, view = 0, tree[0]+1, True
    while index < len(forest[tree[1]]) and view:
        distance += 1
        view = forest[tree[1]][index] < forest[tree[1]][tree[0]]
        index += 1

    return distance

def view_distance_west(forest, tree):
    distance, index, view = 0, tree[0]-1, True
    while index >= 0 and view:
        distance += 1
        view = forest[tree[1]][index] < forest[tree[1]][tree[0]]
        index -= 1

    return distance

def view_distance_total(forest, tree):
    v_north = view_distance_north(forest, tree)
    v_east = view_distance_east(forest, tree)
    v_south = view_distance_south(forest, tree)
    v_west = view_distance_west(forest, tree)
    return v_north*v_east*v_south*v_west
def find_best_tree_for_treehouse(forest):
    max_view_distance = 0
    for y in range(1, len(forest)):
        for x in range(1, len(forest[y])):
            view_distance = view_distance_total(forest, [x, y])
            if view_distance > max_view_distance:
                print(view_distance, x, y)
                max_view_distance = view_distance

    return max_view_distance

def main():
    forest = read_forest_file()
    print(forest)
    visible_trees = count_visible_trees(forest)
    print(visible_trees, " Trees are visible form the sides")

    view_distance = find_best_tree_for_treehouse(forest)
    print("the best view distance for the treehouse is ", view_distance)



if __name__ == "__main__":
    main()