import math

def get_value(string):
    string = string.replace("x=", "").replace("y=", "").replace(",", "").replace(":", "")
    return int(string)


def get_cave_right_size(name):
    max_x, max_y, min_x, min_y, max_distance_x, max_distance_y = 0, 0, math.inf, math.inf, 0, 0
    with open(name, "r") as file:
        for line in file.readlines():
            line = line.strip().split()
            x_1 = get_value(line[2])
            y_1 = get_value(line[3])
            x_2 = get_value(line[8])
            y_2 = get_value(line[9])

            max_x = max(max_x, x_1, x_2)
            min_x = min(min_x, x_1, x_2)
            max_distance_x = max(max_distance_x, max(x_1 - x_2, x_2 - x_1))

            max_y = max(max_y, y_1, y_2)
            min_y = min(min_y, y_1, y_2)
            max_distance_y = max(max_distance_y, max(y_1 - y_2, y_2 - y_1))

    return min_y-max_distance_y, max_y+max_distance_y, min_x-max_distance_x, max_x+max_distance_x

class Sensor:
    x = int
    y = int
    distance = int

    def __init__(self, x, y, distance):
        self.x = x
        self.y = y
        self.distance = distance

    def is_in_distance(self, x, y):
        distance = max(x-self.x, self.x-x) + max(y-self.y, self.y-y)
        return distance <= self.distance

class Beacon:
    x = int
    y = int

    def __init__(self, x, y):
        self.x = x
        self.y = y

    def is_at(self, x, y):
        return self.x == x and self.y == y


def create_sensors_beacons(name):
    sensors, beacons = [], []
    with open(name, "r") as file:
        for line in file.readlines():
            line = line.strip().split()
            x_1 = get_value(line[2])
            y_1 = get_value(line[3])
            x_2 = get_value(line[8])
            y_2 = get_value(line[9])
            distance = max(x_1-x_2, x_2-x_1) + max(y_1-y_2, y_2-y_1)
            sens = Sensor(x_1, y_1, distance)
            sensors.append(sens)
            beac = Beacon(x_2, y_2)
            beacons.append(beac)

    return sensors, beacons


def check_for_line(y, min_x, max_x, sensors, beacons):
    count = 0
    for x in range(min_x, max_x):
        beacon_at = False
        for b in beacons:
            if b.is_at(x, y):
                beacon_at = True
                break

        if beacon_at:
            continue

        for s in sensors:
            if s.is_in_distance(x, y):
                count += 1
                break

    return count


def check_everything(min_x, min_y, max_x, max_y, sensors):
    for y in range(min_y, max_y):
        for x in range(min_x, max_x):
            is_in_range = False
            for s in sensors:
                if s.is_in_distance(x, y):
                    is_in_range = True
                    break
            if is_in_range:
                continue

            return x, y


def get_tuning_frequency(x, y):
    return (x * 4000000) + y


def test_part_1():
    min_y, max_y, min_x, max_x = get_cave_right_size("test_part_1")
    sensors, beacons = create_sensors_beacons("test_part_1")
    count = check_for_line(10, min_x, max_x, sensors, beacons)
    assert count == 26


def test_part_2():
    min_y, max_y, min_x, max_x = get_cave_right_size("test_part_1")
    sensors, beacons = create_sensors_beacons("test_part_1")
    x, y = check_everything(0, 0, 20, 20, sensors)
    frequency = get_tuning_frequency(x, y)
    assert frequency == 56000011

def main():
    min_y, max_y, min_x, max_x = get_cave_right_size("Beacon_input")
    sensors, beacons = create_sensors_beacons("Beacon_input")
    count = check_for_line(2000000, min_x, max_x, sensors, beacons)
    print("part 1: the distress call does not come from ", count, " many fields in this row")

    x, y = check_everything(0, 0, 4000000, 4000000, sensors)
    frequency = get_tuning_frequency(x, y)
    print("part 2: the tuning frequency of the distress beacon is: ", frequency, "because it is positioned at ", x ,", " , y)






if __name__ == "__main__":
    main()