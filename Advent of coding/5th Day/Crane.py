


def read_input():
    containers, movements = dict(), []

    with open("Crane_Input", "r") as file:
        still_containers = True
        count = 0
        for line in file.readlines():
            if line == "\n":
                continue

            elif still_containers:
                if line[1] == "1":
                    still_containers = False
                    continue
                i = 0
                while (i*4 + 1) < len(line):
                    if line[i*4 + 1] != " " and (i+1) in containers:
                        containers[i+1].insert(0, line[1+4*i])
                    if line[i*4 + 1] != " " and not (i+1) in containers:
                        containers[(i+1)] = [line[1+4*i]]
                    if line[i*4 +1] == " " and not (i+1) in containers:
                        containers[(i+1)] = []
                    i += 1

            else:
                words = line.split()
                movements.append([int(words[1]), int(words[3]), int(words[5])])
        return containers, movements



def one_move(containers, move):
    item = containers[move[0]].pop()
    containers[move[1]].append(item)
    return containers

def crame_movements(containers, movements):
    for move in movements:
        counter = move[0]
        while counter > 0:
            containers = one_move(containers, [move[1], move[2]])
            counter -= 1
    return containers

def get_topmost_containers(containers):
    i = 1
    tops = ""
    while i in containers:
        tops += containers[i][-1]
        i += 1
    return tops

def crane_movements_9001(containers, movements):
    for move in movements:
        crane = []
        for i in range(move[0]):
            crane.append(containers[move[1]].pop())
        for i in range(move[0]):
            containers[move[2]].append(crane.pop())
    return containers

def main():
    containers, movements = read_input()
    new_containers_9000 = crame_movements(containers, movements)
    all_tops_9000 = get_topmost_containers(new_containers_9000)
    print(" the topmost containers after moving are: ", all_tops_9000)

    containers, movements = read_input()
    new_containers_9001 = crane_movements_9001(containers, movements)
    all_tops_9001 = get_topmost_containers(new_containers_9001)
    print(" the topmost containers after moving are: ", all_tops_9001)



if __name__ == "__main__":
    main()
