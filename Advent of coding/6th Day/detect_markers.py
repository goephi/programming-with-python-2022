



def read_code():
    with open("marker_input", "r") as file:
        return file.readline()

def check_code(code, length):
    checker = set(code)
    return len(checker) == length

def find_marker_position(code, length):
    code_list, position = [], 0
    for c in code:
        code_list.insert(0, c)
        position += 1
        if len(code_list) > length:
            code_list.pop()
        if check_code(code_list, length):
            break


    return position

def main():
    code = read_code()
    marker_position = find_marker_position(code, 4)
    print("the position of the marker from this code is at: ", marker_position)

    message_marker = find_marker_position(code, 14)
    print("message marker at: ", message_marker)

if __name__ == "__main__":
    main()