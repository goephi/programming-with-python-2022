
def read_file():

    with open("day01.txt", "r") as file:
        max_calories = 0
        max_elf = 1
        calories_count = 0
        elf_count = 1

        for line in file.readlines():
            if line == "\n":
                if calories_count > max_calories:
                    max_calories = calories_count
                    max_elf = elf_count
                calories_count = 0
                elf_count += 1
            else:
                calories_count += int(line)
    return max_elf, max_calories


def read_file_calories_top_3():

    with open("day01.txt", "r") as file:
        max_calories, max_calories_2, max_calories_3 = 0, 0, 0
        max_elf, max_elf_2, max_elf_3 = 1, 1, 1
        calories_count = 0
        elf_count = 1

        for line in file.readlines():
            if line == "\n":
                if calories_count > max_calories:
                    max_calories_3, max_elf_3 = max_calories_2, max_elf_2
                    max_calories_2, max_elf_2 = max_calories, max_elf
                    max_calories, max_elf = calories_count, elf_count
                elif calories_count > max_calories_2:
                    max_calories_3, max_elf_3 = max_calories_2, max_elf_2
                    max_calories_2, max_elf_2 = calories_count, elf_count
                elif calories_count > max_calories_3:
                    max_calories_3, max_elf_3 = calories_count, elf_count

                calories_count = 0
                elf_count += 1
            else:
                calories_count += int(line)

    return max_elf, max_calories, max_elf_2, max_calories_2, max_elf_3, max_calories_3

def main():
    elf, calories = read_file()
    print(elf, "th elf carries ", calories)
    elf, calories, elf_2, calories_2, elf_3, calories_3 = read_file_calories_top_3()
    print(elf, "carries ", calories, "\n", elf_2, "carries ", calories_2, "\n", elf_3, "carries ", calories_3, "\n", "total calories ", calories+calories_2+calories_3)


if __name__ == "__main__":
     main()