import copy


class Valve:
    name = str
    rate = int
    connections = []

    def __init__(self, name, rate, connections):
        self.name = name
        self.rate = rate
        self.connections = connections


class Path:
    start = Valve
    end = Valve
    length = int

    def __init__(self, start, end, length):
        self.start = start
        self.end = end
        self.length = length


class Walker:
    current_position = Valve
    total_release = int
    time = int
    opened = []

    def __init__(self, current_position, totoal_release, time, opened):
        self.current_position = current_position
        self.total_release = totoal_release
        self.time = time
        self.opened = opened

    def use_path(self, path):
        if path.end in self.opened:
            print("We have already been to this valve")
            return
        else:
            self.current_position = path.end
            self.time -= path.length

    def use_valve(self, valve):
        if valve == self.current_position:
            if not valve in self.opened:
                self.time -= 1
                self.total_release += (self.time * valve.rate)
                self.opened.append(valve)
            else:
                print("We already opened this valve")
                return
        else:
            print("we are not at this valve to be opened")
            return


def get_pressure_system(name):
    valves = dict()
    with open(name, "r") as file:
        for line in file.readlines():
            line = line.strip().split()
            name = line[1]
            rate = int(line[4].replace(";", "").replace("rate=", ""))
            connections_strs = line[9:]
            connections = []
            for e in connections_strs:
                connections.append(e.replace(",", ""))

            valve = Valve(name, rate, connections)
            valves.update({name: valve})

    return valves


def find_path(start_valve, end_valve, system):
    queue = copy.copy(start_valve.connections)
    new_queue = []
    length = 1
    while True:
        for e in queue:
            if system[e] == end_valve:
                return length
            else:
                for q in system[e].connections:
                    new_queue.append(q)
        queue = copy.copy(new_queue)
        new_queue = []
        length += 1


def get_paths(system):
    paths = dict()
    non_zero_valves = []
    for valve in system:
        if system[valve].rate > 0:
            non_zero_valves.append(system[valve])

    if system["AA"] not in non_zero_valves:
        non_zero_valves.append(system["AA"])

    for s in non_zero_valves:
        for e in non_zero_valves:
            if s != e:
                if s in paths:
                    paths[s].append(Path(s, e, find_path(s, e, system)))
                else:
                    paths.update({s: [Path(s, e, find_path(s, e, system))]})

    return paths, non_zero_valves


def get_path(paths, start, end):
    for p in paths:
        if p == start:
            for e in paths[p]:
                if e.end == end:
                    return e

    print("no path found")
    return


def best_release(system, paths, non_zero_valves):
    start = system["AA"]
    walkers = [Walker(start, 0, 30, [])]
    max_release = 0

    while len(walkers) > 0:
        walker = walkers.pop(0)
        max_release = max(max_release, walker.total_release)
        if walker.time <= 0:
            continue

        new_walkers = []
        for valve in non_zero_valves:
            if valve == walker.current_position and not valve in walker.opened:
                walker_2 = copy.copy(walker)
                walker_2.opened = copy.copy(walker.opened)
                walker_2.use_valve(valve)
                new_walkers.append(walker_2)

            else:
                if not valve in walker.opened:
                    path = get_path(paths, walker.current_position, valve)
                    if path.length < walker.time:
                        walker_3 = copy.copy(walker)
                        walker_3.use_path(path)
                        new_walkers.append(walker_3)

        for w in new_walkers:
            walkers.append(w)

    return max_release


def print_paths(paths):
    for s in paths:
        for e in paths[s]:
            print(e.start.name, e.end.name, e.length)


def test_part_1():
    system = get_pressure_system("test_part_1")
    paths, non_zero_valves = get_paths(system)
    print_paths(paths)
    release = best_release(system, paths, non_zero_valves)

    assert release == 1651


def main():
    system = get_pressure_system("day16.txt")
    paths, non_zero_valves = get_paths(system)
    release = best_release(system, paths, non_zero_valves)
    print("part 1: release is ", release)


if __name__ == "__main__":
    main()
