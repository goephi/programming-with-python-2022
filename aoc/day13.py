def read_pairs_from_file(name):
    pairs, index = dict(), 1

    with open(name, "r") as file:
        while True:
            left, right = file.readline().strip(), file.readline().strip()

            pairs.update({index: [left, right]})
            index += 1
            if file.readline() != "\n":
                return pairs


def get_next_token(index, line):
    token = ""
    if line[index] == ",":
        index += 1
    if line[index] == "[":
        return "[", index + 1
    if line[index] == "]":
        return "]", index + 1
    while line[index] != "[" and line[index] != "]" and line[index] != ",":
        token += line[index]
        index += 1
    return token, index

def compare_left_list_to_right_int(left, left_index, right_token):
    left_token, left_index = get_next_token(left_index, left)
    while left_token != "]" and left_index < len(left):
        if left_token == "[":
            ordered, left_index = compare_left_list_to_right_int(left, left_index, right_token)
            if ordered:
                continue
            else:
                return False, left_index
        elif right_token == "]":
            return True, left_index
        else:
            if int(left_token) > int(right_token):
                return False, left_index
        left_token, left_index = get_next_token(left_index, left)

    return True, left_index


def compare_right_list_to_left_int(right, right_index, left_token):
    right_token, right_index = get_next_token(right_index, right)
    while right_token != "]" and right_index < len(right):
        if right_token == "[":
            ordered, right_index = compare_right_list_to_left_int(right, right_index, left_token)
            if ordered:
                continue
            else:
                return False, right_index
        elif left_token == "]":
            return True, right_index
        else:
            if int(left_token) > int(right_token):
                return False, right_index
        right_token, right_index = get_next_token(right_index, right)

    return True, right_index

def in_order(left, right):
    left_index, right_index = 0, 0

    while left_index < len(left):
        if right_index >= len(right):
            return False

        left_token, left_index = get_next_token(left_index, left)
        right_token, right_index = get_next_token(right_index, right)

        if right_token == "]":
            if left_token != "]":
                return False

        if left_token == "[":
            if right_token != "[":
                ordered, left_index = compare_left_list_to_right_int(left, left_index, right_token)
                if ordered:
                    continue
                else:
                    return False
            if right_token == "[":
                continue

        if right_token == "[":
            if left_token != "[":
                ordered, right_index = compare_right_list_to_left_int(right, right_index, left_token)
                if ordered:
                    continue
                else:
                    return False

        if left_token == "]":
            if right_token != "]":
                right_index = right_index - len(right_token)
                continue
            if right_token == "]":
                continue

        if int(left_token) > int(right_token):
            return False

    return True


def compare_the_pairs(pairs):
    summe = 0
    for e in pairs:
        if in_order(pairs[e][0], pairs[e][1]):
            summe += e
    return summe


def test_pairs_compare():
    test_pairs = read_pairs_from_file("test_input_pairs")
    assert in_order(test_pairs[1][0], test_pairs[1][1])
    assert in_order(test_pairs[2][0], test_pairs[2][1])
    assert not in_order(test_pairs[3][0], test_pairs[3][1])
    assert in_order(test_pairs[4][0], test_pairs[4][1])
    assert not in_order(test_pairs[5][0], test_pairs[5][1])
    assert in_order(test_pairs[6][0], test_pairs[6][1])
    assert not in_order(test_pairs[7][0], test_pairs[7][1])
    assert not in_order(test_pairs[8][0], test_pairs[8][1])


def test_summe():
    test_pairs = read_pairs_from_file("test_input_pairs")
    test_summe = compare_the_pairs(test_pairs)
    assert test_summe == 13


def main():
    pairs = read_pairs_from_file("day13.txt")
    print(pairs)
    summe = compare_the_pairs(pairs)
    print(summe)


if __name__ == "__main__":
    main()
