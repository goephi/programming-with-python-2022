def read_file_to_rucksack_list():
    with open("day03.txt", "r") as file:
        rucksack_list = file.readlines()
    return rucksack_list


def find_item_type_in_both_compartments_in_rucksack(rucksack):
    compartment_1 = rucksack[0: len(rucksack) // 2]
    compartment_2 = rucksack[len(rucksack) // 2: len(rucksack)]

    for c in compartment_1:
        if c in compartment_2:
            return c

    return False


def list_all_duplicate_types(rucksacks):
    type_list = []
    for r in rucksacks:
        type_list.append(find_item_type_in_both_compartments_in_rucksack(r))

    return type_list


def get_type_priority(c):
    num = ord(c)

    if 65 <= num <= 90:
        return num - 38
    elif 97 <= num <= 122:
        return num - 96

    return False


def add_over_all_types(type_list):
    sum = 0
    for t in type_list:
        sum += get_type_priority(t)

    return sum


def find_group_badge(a, b, c):
    for character in a:
        if character in b and character in c:
            return character
    return False


def find_all_badges(elfs):
    badges = []
    i = 0
    while i < len(elfs):
        a = elfs[i]
        b = elfs[i + 1]
        c = elfs[i + 2]
        badges.append(find_group_badge(a, b, c))
        i += 3
    return badges


def main():
    rucksacks = read_file_to_rucksack_list()
    type_list = list_all_duplicate_types(rucksacks)
    priority_sum = add_over_all_types(type_list)
    print("the total priority sum is: ", priority_sum)
    badge_list = find_all_badges(rucksacks)
    badge_sum = add_over_all_types(badge_list)
    print("the total badge priority is: ", badge_sum)


if __name__ == "__main__":
    main()
