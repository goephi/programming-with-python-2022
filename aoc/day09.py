
def get_rope_walk_from_file(name):
    walk = []
    with open(name, "r") as file:
        for line in file.readlines():
            walk.append(line.strip().split())
    return walk

def update_tail(head, tail):
    distance_x = head[0]-tail[0]
    distance_y = head[1]-tail[1]

    if distance_x == 0:
        if distance_y == 2:
            tail[1] += 1
        if distance_y == -2:
            tail[1] -= 1
    if distance_x == 1:
        if distance_y == 2:
            tail[0] += 1
            tail[1] += 1
        if distance_y == -2:
            tail[0] += 1
            tail[1] -= 1
    if distance_x == -1:
        if distance_y == 2:
            tail[0] -= 1
            tail[1] += 1
        if distance_y == -2:
            tail[0] -= 1
            tail[1] -= 1
    if distance_x == 2:
        tail[0] += 1
        if distance_y == 1 or distance_y == 2:
            tail[1] += 1
        if distance_y == -1 or distance_y == -2:
            tail[1] -= 1
    if distance_x == -2:
        tail[0] -= 1
        if distance_y == 1 or distance_y == 2:
            tail[1] += 1
        if distance_y == -1 or distance_y == -2:
            tail[1] -= 1

    return tail

def perform_step(direction, head, tail):
    if direction == "U":
        head[1] += 1
    if direction == "D":
        head[1] -= 1
    if direction == "L":
        head[0] -= 1
    if direction == "R":
        head[0] += 1
    tail = update_tail(head, tail)
    return head, tail

def get_size_positions(positions):
    count = 0
    for element in positions:
        count += len(positions[element])
    return count

def perform_rope_walk(walk):
    head, tail, positions = [0, 0], [0, 0], dict()

    for step in walk:
        for times in range(int(step[1])):
            head, tail = perform_step(step[0], head, tail)
            if tail[0] in positions:
                positions[tail[0]].add(tail[1])
            else:
                positions.update({tail[0]: {tail[1]}})

    return get_size_positions(positions)

def perform_step_large_rope(direction, rope):
    if direction == "U":
        rope[0][1] += 1
    if direction == "D":
        rope[0][1] -= 1
    if direction == "L":
        rope[0][0] -= 1
    if direction == "R":
        rope[0][0] += 1

    index = 0
    while index < len(rope)-1:
        rope[index+1] = update_tail(rope[index], rope[index+1])
        index += 1
    return rope

def perform_rope_walk_large_rope(walk, rope_length):
    rope, positions = [], dict()
    for i in range(rope_length):
        rope.append([0, 0])

    for step in walk:
        for times in range(int(step[1])):
            rope = perform_step_large_rope(step[0], rope)
            if rope[-1][0] in positions:
                positions[rope[-1][0]].add(rope[-1][1])
            else:
                positions.update({rope[-1][0]: {rope[-1][1]}})

    return get_size_positions(positions)

def test_large_rope_walk():
    walk = get_rope_walk_from_file("test_imput")
    assert perform_rope_walk_large_rope(walk, 10) == 36
def main():
    rope_walk = get_rope_walk_from_file("day09.txt")
    num_positions = perform_rope_walk(rope_walk)
    print("the tail of the rope visited ", num_positions, " at least once during the rope walk.")

    num_positions_large_rope = perform_rope_walk_large_rope(rope_walk, 10)
    print("the tail of the large rope visited ", num_positions_large_rope, " at least once during the large rope walk.")

if __name__ == "__main__":
    main()