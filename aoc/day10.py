
def read_file_to_signal(file_name):
    signal = []
    with open(file_name, "r") as file:
        for line in file.readlines():
            signal.append(line.strip().split())

    return signal

def get_values_out_of_signal(signals, values, runtime):
    of_interest = []
    cycle, action, value, signal_count, read_new_signal = 0, 0, 1, 0, True

    for index in range(1, runtime+1):
        if cycle <= 0:
            value += action
            action = 0
            read_new_signal = True
        else:
            cycle -= 1

        if index in values:
            of_interest.append(value)

        if read_new_signal:
            if signals[signal_count][0] == "noop":
                signal_count += 1

            elif signals[signal_count][0] == "addx":
                cycle = 1
                action = int(signals[signal_count][1])
                signal_count += 1
                read_new_signal = False

    return of_interest

def display_controle(signals, runtime):
    output = [[]]
    cycle, action, sprite_value, signal_count, read_new_signal = 0, 0, 1, 0, True
    output_row = 0

    for index in range(1, runtime + 1):
        if cycle <= 0:
            sprite_value += action
            action = 0
            read_new_signal = True
        else:
            cycle -= 1

        if (index-1) % 40 == 0 and index != 1:
            output_row += 1
            output.append([])
        if sprite_value - 1 <= (index - 1) % 40 <= sprite_value + 1:
            output[output_row].append("#")
        else:
            output[output_row].append(".")


        if read_new_signal:
            if signals[signal_count][0] == "noop":
                signal_count += 1

            elif signals[signal_count][0] == "addx":
                cycle = 1
                action = int(signals[signal_count][1])
                signal_count += 1
                read_new_signal = False

    return output

def calc_sum_return_values(values, multiplicants):
    sum = 0
    for i in range(len(values)):
        sum += (values[i]*multiplicants[i])

    return sum

def transform_output(output):
    string = ""
    for row in output:
        for element in row:
            string += element
        string += '\n'

    return string

def test_get_values():
    test_signal = read_file_to_signal("test_signal_input")
    test_important_values = [20, 60, 100, 140, 180, 220]
    test_values = get_values_out_of_signal(test_signal, test_important_values, 220)
    assert test_values == [21, 19, 18, 21, 16, 18]
    test_sum = calc_sum_return_values(test_values, test_important_values)
    assert test_sum == 13140

def test_output():
    test_signal = read_file_to_signal("test_signal_input")
    test_out = display_controle(test_signal, 240)
    test_print = transform_output(test_out)
    test_result = "##..##..##..##..##..##..##..##..##..##..\n###...###...###...###...###...###...###.\n####....####....####....####....####....\n#####.....#####.....#####.....#####.....\n######......######......######......####\n#######.......#######.......#######.....\n"
    print(test_print, "\n", test_result)
    print(len(test_print), len(test_result))
    assert test_result == test_print


def main():
    signals = read_file_to_signal("day10.txt")

    important_values = [20, 60, 100, 140, 180, 220]
    signal_values = get_values_out_of_signal(signals, important_values, 220)
    signal_sum = calc_sum_return_values(signal_values, important_values)
    print("the sum of the signals is ", signal_sum)

    out_put = display_controle(signals, 240)
    out_print = transform_output(out_put)
    print(out_print)


if __name__ == "__main__":
    main()