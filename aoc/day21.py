import math
import sys


class Expression:
    simple = bool
    value = float
    first = str
    second = str
    operation = str

    def __init__(self, simple, value, first, second, operation):
        self.simple = simple
        self.value = value
        self.first = first
        self.second = second
        self.operation = operation

    def eval(self, expressions):
        if self.simple:
            return self.value
        else:
            first_value = expressions[self.first].eval(expressions)
            second_value = expressions[self.second].eval(expressions)

            if self.operation == "+":
                return first_value + second_value
            elif self.operation == "-":
                return first_value - second_value
            elif self.operation == "*":
                return first_value * second_value
            elif self.operation == "/":
                return first_value / second_value
            else:
                print("creazy operation ", str(self.operation))


def read_file_to_Expressions(name):
    expresssions = dict()
    with open(name, "r") as file:
        for line in file.readlines():
            line = line.strip().split()

            monkey = line[0].replace(":", "")

            if len(line) > 2:
                simple = False
                value = 0
                first = line[1]
                second = line[3]
                operation = line[2]
                expression = Expression(simple, value, first, second, operation)
                expresssions.update({monkey: expression})

            else:
                simple = True
                value = float(line[1])
                expression = Expression(simple, value, None, None, None)
                expresssions.update({monkey: expression})

    return expresssions


def evaluate_monkey_screams(expressions):
    return expressions["root"].eval(expressions)


def equality_monkeys(expressions):
    return

def test_part1():
    expressions = read_file_to_Expressions("test_input")
    result = evaluate_monkey_screams(expressions)
    assert result == 152


def test_part2():
    expressions = read_file_to_Expressions("test_input")
    result = equality_monkeys(expressions)
    assert result == 301


def main():
    # sys.setrecursionlimit(1500)
    expressions = read_file_to_Expressions("day21.txt")
    result = evaluate_monkey_screams(expressions)
    print("the result of the monkey screams in part one is ", result)

    result_part_2 = equality_monkeys(expressions)
    print("the number that I have to scream in part 2 is ", result_part_2)
    print(expressions[expressions["root"].first].eval(expressions) == expressions[expressions["root"].second].eval(
        expressions))


if __name__ == "__main__":
    main()
