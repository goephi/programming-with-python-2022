



def read_cleaning_file():
    pairs = []
    with open("day04.txt","r") as file:
        for line in file.readlines():
            first, second = line.strip().split(",")
            pairs.append([list(first.split("-")), list(second.split("-"))])
    return pairs

def get_intervall(item):
    smaller = int(item[0])
    bigger = int(item[1])
    return smaller, bigger

def intervall_in_the_other(pair):
    first_s, first_b = get_intervall(pair[0])
    second_s, second_b = get_intervall(pair[1])
    if first_s <= second_s and first_b >= second_b:
        return True
    if second_s <= first_s and second_b >= first_b:
        return True
    return False

def intervalls_overlap_at_all(pair):
    first_s, first_b = get_intervall(pair[0])
    second_s, second_b = get_intervall(pair[1])
    if first_s >= second_s and first_s <=second_b:
        return True
    if first_b >= second_s and first_b <= second_b:
        return True
    if first_s <= second_s and first_b >= second_b:
        return True
    return False

def count_over_all_pairs(pairs):
    count = 0
    for pair in pairs:
        if intervall_in_the_other(pair):
            count += 1

    return count

def count_over_all_pairs_partial_overlapp(pairs):
    count = 0
    for pair in pairs:
        if intervalls_overlap_at_all(pair):
            count += 1

    return count
def main():
    pairings = read_cleaning_file()
    overlaps = count_over_all_pairs(pairings)
    print("the number ov pairs with aximum Overlap is: ", overlaps)

    overlaps_at_all = count_over_all_pairs_partial_overlapp(pairings)
    print("the number of pairs with an Overlap is: ", overlaps_at_all)

if __name__ == "__main__":
    main()

