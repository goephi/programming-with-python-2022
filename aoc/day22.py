class Walker:
    x = int
    y = int
    direction = int

    # Facing is 0 for right (>), 1 for down (v), 2 for left (<), and 3 for up (^)

    def __init__(self, x, y):
        self.x = x
        self.y = y
        self.direction = 0

    def perform_steps(self, number, board):
        while number > 0:
            if self.direction == 0:
                if self.x + 1 >= len(board[self.y]) or board[self.y][self.x + 1] == "o":
                    x, walkable = find_first_x_in_row(board, self.y)
                    if walkable:
                        self.x = x
                        number -= 1
                    else:
                        return
                elif board[self.y][self.x + 1] == ".":
                    self.x += 1
                    number -= 1
                elif board[self.y][self.x + 1] == "#":
                    return

            if self.direction == 2:
                if self.x - 1 < 0 or board[self.y][self.x - 1] == "o":
                    x, walkable = find_last_x_in_row(board, self.y)
                    if walkable:
                        self.x = x
                        number -= 1
                    else:
                        return
                elif board[self.y][self.x - 1] == ".":
                    self.x -= 1
                    number -= 1
                elif board[self.y][self.x - 1] == "#":
                    return

            if self.direction == 1:
                if self.y + 1 >= len(board) or self.x >= len(board[self.y+1]) or board[self.y + 1][self.x] == "o":
                    y, walkable = find_first_y_in_column(board, self.x)
                    if walkable:
                        self.y = y
                        number -= 1
                    else:
                        return
                elif board[self.y + 1][self.x] == ".":
                    self.y += 1
                    number -= 1
                elif board[self.y + 1][self.x] == "#":
                    return

            if self.direction == 3:
                if self.y - 1 < 0 or board[self.y - 1][self.x] == "o":
                    y, walkable = find_last_y_in_column(board, self.x)
                    if walkable:
                        self.y = y
                        number -= 1
                    else:
                        return
                elif board[self.y - 1][self.x] == ".":
                    self.y -= 1
                    number -= 1
                elif board[self.y - 1][self.x] == "#":
                    return

    def turn(self, direction):
        if direction == "L":
            self.direction -= 1
            if self.direction == -1:
                self.direction = 3
        if direction == "R":
            self.direction += 1
            if self.direction == 4:
                self.direction = 0

    def eval_part_1(self):
        return 1000 * (self.y + 1) + 4 * (self.x + 1) + self.direction


def find_first_x_in_row(board, row):
    for x in range(len(board[row])):
        if board[row][x] == "#":
            return [x, False]
        if board[row][x] == ".":
            return [x, True]


def find_last_x_in_row(board, row):
    for x in range(len(board[row]) - 1, -1, -1):
        if board[row][x] == "#":
            return x, False
        if board[row][x] == ".":
            return x, True


def find_first_y_in_column(board, column):
    for y in range(len(board)):
        if board[y][column] == "#":
            return y, False
        if board[y][column] == ".":
            return y, True


def find_last_y_in_column(board, column):
    for y in range(len(board) - 1, -1, -1):
        if column > len(board[y]):
            continue
        if board[y][column] == "#":
            return y, False
        if board[y][column] == ".":
            return y, True


def read_file_to_board_and_walk(name):
    board = []
    read_board = True
    with open(name, "r") as file:
        for line in file.readlines():
            if line == "\n":
                read_board = False
                continue
            if read_board:
                line = line.replace(" ", "o").strip()
                board.append(line)
            else:
                walk = line.strip()

    return board, walk


def get_next_value(walk, position):
    if position >= len(walk):
        return
    if walk[position] == "R":
        return "R", position + 1, True
    if walk[position] == "L":
        return "L", position + 1, True
    number = ""
    while position < len(walk) and walk[position] != "L" and walk[position] != "R":
        number += walk[position]
        position += 1
    return number, position, False


def start_walking(board, walk):
    start_x, possible = find_first_x_in_row(board, 0)
    walker = Walker(start_x, 0)
    position = 0

    while position < len(walk):
        print(walker.x, walker.y, walker.direction)
        value, position, turn = get_next_value(walk, position)
        if turn:
            walker.turn(value)
        else:
            walker.perform_steps(int(value), board)

    return walker


def test_part_1():
    board, walk = read_file_to_board_and_walk("test_file")
    walker = start_walking(board, walk)
    assert walker.eval_part_1() == 6032

def main():
    board, walk = read_file_to_board_and_walk("day22.txt")
    walker = start_walking(board, walk)
    print("part one eval = ", walker.eval_part_1())


if __name__ == "__main__":
    main()
