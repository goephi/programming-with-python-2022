import pandas as pd
import itertools

#A = Rock
#B = Paper
#C = Scissors

#win 6, lose 0, draw 3
def opponent_vs_me(opp, mine):
    if opp == 'A':
        if mine == 'A':
            return 3
        elif mine == 'B':
            return 6
        else:
            return 0
    elif opp == 'B':
        if mine == 'A':
            return 0
        elif mine == 'B':
            return 3
        else:
            return 6
    else:
        if mine == 'A':
            return 6
        elif mine == 'B':
            return 0
        else:
            return 3

#1 for Rock, 2 for Paper, and 3 for Scissors
def score_by_sign(mine):
    if mine == 'A':
        return 1
    elif mine == 'B':
        return 2
    else:
        return 3

#calculates the score for a specified stratagy
def caclulate_score(matrix, x, y, z):
    score = 0
    for opp in matrix:
        score += matrix[opp]['X']*(score_by_sign(x)+opponent_vs_me(opp, x))
        score += matrix[opp]['Y']*(score_by_sign(y)+opponent_vs_me(opp, y))
        score += matrix[opp]['Z']*(score_by_sign(z)+opponent_vs_me(opp, z))

    return score


#evaluates the stratagies for x,y,z == random
def eval_stratagy(matrix):
    permutations = list(itertools.permutations(["A", "B", "C"]))

    max_score, max_permutation = 0, []
    for e in permutations:
        score = caclulate_score(matrix, e[0], e[1], e[2])
        if max_score < score:
            max_score = score
            max_permutation = e

    return max_permutation, max_score

#for stratagy in assignment
def example(matrix):
    return caclulate_score(matrix, "A", "B", "C")


# gives a matrix with all round counted
def read_stratagy_guid():
    d = {'A': [0, 0, 0], 'B': [0, 0, 0], 'C':[0, 0, 0]}
    matrix = pd.DataFrame(data=d, index=["X", "Y", "Z"])

    with open("day02.txt", "r") as file:
        for line in file.readlines():
            opponent, mine = line.split()
            matrix[opponent][mine] += 1

    return matrix


def get_win_vs_opp(opp):
    if opp == 'A':
        return 'B'
    elif opp == 'B':
        return 'C'
    else:
        return 'A'

def get_draw_vs_opp(opp):
    return opp

def get_loss_vs_opp(opp):
    if opp == 'A':
        return 'C'
    elif opp == 'B':
        return 'A'
    else:
        return 'B'

#fun for part 2 where the second row is the outcome
#X means lose, Y means draw, and Z means win
def stratagy_2nd_row_outcomes(matrix):
    score = 0
    for opp in matrix:
        score += (matrix[opp][0] * (0 + score_by_sign(get_loss_vs_opp(opp))))
        score += (matrix[opp][1] * (3 + score_by_sign(get_draw_vs_opp(opp))))
        score += (matrix[opp][2] * (6 + score_by_sign(get_win_vs_opp(opp))))

    return score

def main():
    matrix = read_stratagy_guid()
    mapping, score = eval_stratagy(matrix)
    print("The best stratagie is: X=", mapping[0], ", Y=", mapping[1], ", Z=", mapping[2], " with a total score of: ", score)

    print(" score form example: ", example(matrix))

    print("Score if second column represents the win, draw, lose eval: ", stratagy_2nd_row_outcomes(matrix))

if __name__ == "__main__":
     main()