
def my_max(my_list):
    max_value = my_list[0]
    for e in my_list:
        if e > max_value:
            max_value = e
    return max_value


def process_list(my_list):
    for e in range(len(my_list)):
        if my_list[e] % 7 == 0:
            my_list[e] += e
        elif my_list[e] % 2 == 0:
            my_list[e] /= 2
        else:
            my_list[e] *= 2
    return my_max(my_list)


def collatz(num):
    my_list = []

    while num not in my_list:
        if num % 2 == 0:
            num /= 2
        else:
            num *= 3
            num += 1
        my_list.append(num)

    return my_list[-1]

def compute_pi():
    pi = 1
    e = 1
    while e < 100:
        pi -= (1 /(e * 2 + 1))
        e += 1
        pi += (1 /(e * 2 + 1))
        e += 1
    return pi*4


def integer_cube_root(num):
    for e in range(num):
        if e*e*e >= num:
            return e
    return e

def caeser(word, num):
    new_word = ""
    for e in word:
        if e ==' ':
            new_word += " "
        else :
            id = ord(e)
            id += num
            if id > 90:
                id -= 26
            new_word += chr(id)
    return new_word

def reverse_complement(DNA):
    DNA_2 = ""
    for e in DNA:
        if e == "A":
            DNA_2 = "T" + DNA_2
        if e == "T":
            DNA_2 = "A" + DNA_2
        if e == "G":
            DNA_2 = "C" + DNA_2
        if e == "C":
            DNA_2 = "G" + DNA_2
    return DNA_2

def insertion_sort(my_list):
    index = 0
    while index < len(my_list):
        i = index - 1
        save_i = index
        while i >= 0:
            if my_list[index] <= my_list[i]:
                save_i = i
                i -= 1
            else:
                break
        my_list.insert(save_i, my_list[index])
        del my_list[index + 1]
        index += 1

    return my_list

def first_n_fibonacci(n):
    fibonacci = [1, 1]
    if n == 0:
        return [1]
    if n == 1:
        return [1]
    if n == 2:
        return [1, 1]
    for e in range(n-2):
        fibonacci.append(fibonacci[-1]+fibonacci[-2])
    return fibonacci



def main():
    my_list = [2, 3, 5, 5, 2, 1, 5, 3, 6, 7, 8, 5, 2, 9]
    print(my_max(my_list))
    print(process_list(my_list))
    print(collatz(6))
    print(compute_pi())
    print(integer_cube_root(11999))
    print(caeser("HALLO IHR LIEBEN", 6))
    print(caeser("NGRRU ONX ROKHKT", 20))
    print(reverse_complement("AATTGGCCTAGCTACG"))
    print(insertion_sort([4,3,2,4,5,1,1,7,8,4,3,5,7,9,9,7,54,43,2,3,45,6,7,87,65,4]))
    print(first_n_fibonacci(12))

if __name__ == "__main__":
     main()