

def cleanUp(data):
    parts = data.split('\n')
    sequence = ""
    for e in range(1, len(parts)):
        sequence += parts[e]
    return sequence

def Basepairs(sequence):
    count = [0]*4
    for base in sequence:
        if base == "A":
            count[0] += 1
        if base == "C":
            count[1] += 1
        if base == "G":
            count[2] += 1
        if base == "T":
            count[3] += 1
    return count

def CG_content(sequence):
    counter = 0
    for i in range(len(sequence)-1):
        if sequence[i] == "G" and sequence[i+1] == "C":
            counter += 1
    return counter

def number_of_unique_kmers(sequence, k):
    k_mers = dict()
    for index in range(len(sequence)-k+1):
        sub_sequence = sequence[index:index+k]
        if sub_sequence in k_mers:
            k_mers[sub_sequence] += 1
        else:
            k_mers.update({sub_sequence: 1})

    count = 0
    for e in k_mers:
        if k_mers[e] == 1:
            count += 1
    return count

def kmer_code(kmer):
    value = 0
    for index in range(len(kmer)):
        if kmer[index] == "A":
            value += 0
        elif kmer[index] == "C":
            value += (4**(len(kmer)-index-1))
        elif kmer[index] == "G":
            value += (2*4**(len(kmer)-index-1))
        elif kmer[index] == "T":
            value += (3*4**(len(kmer)-index-1))
    return value

def reverse_complement(DNA):
    DNA_2 = ""
    for e in DNA:
        if e == "A":
            DNA_2 = "T" + DNA_2
        if e == "T":
            DNA_2 = "A" + DNA_2
        if e == "G":
            DNA_2 = "C" + DNA_2
        if e == "C":
            DNA_2 = "G" + DNA_2
    return DNA_2

def canonical_code(kmer):
    value_1 = kmer_code(kmer)
    value_2 = kmer_code(reverse_complement(kmer))
    return max(value_1, value_2)


def test_cleanUp():
    assert cleanUp("aswd\najaj\nababa") == "ajajababa"

def test_Basepairs():
    assert Basepairs("AACCGGTT") == [2, 2, 2, 2]
    assert Basepairs("AAACGGTTTT") == [3, 1, 2, 4]

def test_CG_content():
    assert CG_content("GCGCGCGC") == 4
    assert CG_content("AGCTATATCGTATAGCTATATACGTATAGC") == 3

def test_number_of_unique_kmers():
    assert number_of_unique_kmers("AGGCGTAT", 3) == 6
    assert number_of_unique_kmers("AAAAAAAA", 2) == 0
    assert number_of_unique_kmers("AAATTTGGGCCC", 2) == 3

def test_kmer_code():
    assert kmer_code("ACGT") == 27
    assert kmer_code("T") == 3
    assert kmer_code("AAAAAAA") == 0

def test_canonical_code():
    assert canonical_code("AAAC") == 191
    assert canonical_code("GTTT") == 191


def main():
    file = open("Data")
    sequence = file.read()
    sequence = cleanUp(sequence)
    print(Basepairs(sequence))
    print(CG_content(sequence))
    print(number_of_unique_kmers("AAGTACGACGACTCACCGTACGTACGTACGTACGGACGT",3))



if __name__ == "__main__":
     main()