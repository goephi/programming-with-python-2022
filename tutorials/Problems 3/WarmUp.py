


def count_characters_occur_more_than_once_case_insensitive(word):
    counting = [0]*26
    for letter in word:
        ascii = ord(letter)
        if 65 <= ascii <= 90:
            counting[ascii-65] += 1
        if 97 <= ascii <= 122:
            counting[ascii-97] += 1

    value = 0
    for e in counting:
        if e > 1:
            value += 1
    return value

def multiplicative_persistence(num, counter):
    if num < 10:
        return counter
    counter += 1
    digits = []
    while num > 10:
        digits.append(num % 10)
        num //= 10
    for e in digits:
        num *= e
    return multiplicative_persistence(num, counter)


def test_999():
    assert multiplicative_persistence(999, 0) == 4


def main():
    word1, word2, word3 = "abcde", "abBcdea", "counting"
    print(count_characters_occur_more_than_once_case_insensitive(word1))
    print(count_characters_occur_more_than_once_case_insensitive(word2))
    print(count_characters_occur_more_than_once_case_insensitive(word3))

    print(multiplicative_persistence(39,0))
    print(multiplicative_persistence(999,0))
    print(multiplicative_persistence(8,0))




if __name__ == "__main__":
     main()