

def mod_7_my_num(element):
    return element % 7

def mod_6_my_num(element):
    return element % 6

def SORT_THE_LIIIIIIIIIST(list):
    list.sort(reverse=True, key=mod_7_my_num)
    return list

def sort_list_sustom_mod(list, modulator = mod_7_my_num):
    list.sort(reverse=True, key=modulator)
    return list

def test_sort():
    assert SORT_THE_LIIIIIIIIIST([1,2,3,4,5,6,7]) == [6,5,4,3,2,1,7]
    assert sort_list_sustom_mod([1,2,3,4,5,6,7]) == [6,5,4,3,2,1,7]
    assert sort_list_sustom_mod([1,2,3,4,5,6,7], mod_6_my_num) == [5,4,3,2,1,7,6]


def main():
    sorted_list = SORT_THE_LIIIIIIIIIST([1,54,7,9,7,5,5,34,655,35,63,87,876,564,54,73,34,34,53,7,764,8,7,63,2,5,5464,375,68,7,65,37,34])
    print(sorted_list)
    sorted_list = sort_list_sustom_mod(
        [1, 54, 7, 9, 7, 5, 5, 34, 655, 35, 63, 87, 876, 564, 54, 73, 34, 34, 53, 7, 764, 8, 7, 63, 2, 5, 5464, 375, 68,
         7, 65, 37, 34])
    print(sorted_list)

if __name__ == "__main__":
     main()