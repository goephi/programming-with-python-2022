

def factorial():
    n_fac = 1
    n_count = 0
    while True:
        yield n_fac
        n_count += 1
        n_fac = n_count*n_fac

def triangular():
    old_T = 0
    n = 0
    while True:
        yield old_T
        n += 1
        old_T = old_T+n

def convert_bin_to_int(binary):
    digits = list()
    for e in binary:
        digits.append(ord(e)-48)
    value = 0
    for e in range(len(digits)):
        value += digits[e]*2**(len(digits)-e-1)
    return value

def read_code():
    encoding = dict()
    with open("cipher.txt.txt", "r") as file:
        for e in file.readlines():
            character, binary = str.split(e, '\t')
            encoding[character] = convert_bin_to_int(binary)
    return encoding

def test_bin_to_int():
    assert convert_bin_to_int("1100") == 12
    assert convert_bin_to_int("0011") == 3
    assert convert_bin_to_int("0000000000001") == 1
    assert convert_bin_to_int("11111") == 31

def test_factorial():
    factorials = factorial()
    assert next(factorials) == 1
    assert next(factorials) == 1
    assert next(factorials) == 2
    assert next(factorials) == 6
    assert next(factorials) == 24

def test_triangular():
    trianglularis = triangular()
    assert next(trianglularis) == 0
    assert next(trianglularis) == 1
    assert next(trianglularis) == 3
    assert next(trianglularis) == 6
    assert next(trianglularis) == 10

def main():
    factorials = factorial()
    for i in range(10):
        print(next(factorials))
    triangularis = triangular()
    for i in range(10):
        print(next(triangularis))

    encoder = read_code()


if __name__ == "__main__":
     main()